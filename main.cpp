/*
 * main.cpp
 *
 *  Created on: Jun 11, 2017
 *      Author: roland
 */

#include <Arduino.h>

#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>

#include <AD9851.h>

AD9851 ad9851(9, 10, 8, 7);

LiquidCrystal_PCF8574 lcd(0x3f);

#define PIN_ENC_CLK 4
#define PIN_ENC_DT 5
#define PIN_ENC_BUT 3

uint32_t freq = 10000000;
enum {SET_FREQ = 0, SET_STEP} mode;
uint32_t step = 1000;

void setup()
{
	pinMode(PIN_ENC_CLK, INPUT_PULLUP);
	pinMode(PIN_ENC_DT, INPUT_PULLUP);
	pinMode(PIN_ENC_BUT, INPUT_PULLUP);

	Serial.begin(9600);
	lcd.begin(16, 2);
	lcd.setBacklight(1);
	lcd.home();
	lcd.clear();
	lcd.print("*");
	lcd.setCursor(2, 0);
	lcd.print(freq);
	lcd.setCursor(2, 1);
	lcd.print(step);

	ad9851.setFrequency(freq);
	ad9851.sync();
}

void loop()
{
	bool curClk = digitalRead(PIN_ENC_CLK);
	bool curDt = digitalRead(PIN_ENC_DT);
	static bool prevClk = true;
	bool leftTurn = false;
	bool rightTurn = false;
	bool curBut = digitalRead(PIN_ENC_BUT);
	static bool prevBut = true;
	bool butPushed = false;
	bool lcdUpdate = false;

	if(prevClk && !curClk)
	{
		if(curDt)
			leftTurn = true;
		else
			rightTurn = true;
	}
	prevClk = curClk;

	if(curBut && !prevBut)
		butPushed = true;

	prevBut = curBut;

	if(butPushed)
	{
		if(mode == SET_FREQ)
			mode = SET_STEP;
		else
			mode = SET_FREQ;

		lcdUpdate = true;
	}

	if(mode == SET_FREQ)
	{
		if(leftTurn && freq >= 0)
		{
			freq -= step;
			lcdUpdate = true;
			ad9851.setFrequency(freq);
			ad9851.sync();
		}

		if(rightTurn && (freq + step < 70000000))
		{
			freq += step;
			lcdUpdate = true;
			ad9851.setFrequency(freq);
			ad9851.sync();
		}


	}
	else
	{
		if(leftTurn && step > 1)
		{
			step /= 10;
			lcdUpdate = true;
		}

		if(rightTurn && step < 1000000)
		{
			step *= 10;
			lcdUpdate = true;
		}
	}

	delay(10);
	Serial.print(freq);
	Serial.print(" ");
	Serial.println(step);

	if(lcdUpdate)
	{
		lcd.home();
		lcd.clear();
		if(mode == SET_STEP)
			lcd.setCursor(0, 1);

		lcd.print("*");
		lcd.setCursor(2, 0);
		lcd.print(freq);
		lcd.setCursor(2, 1);
		lcd.print(step);
	}
}

